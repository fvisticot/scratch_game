import 'dart:async';
import 'dart:math';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:scratcher/scratcher.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  MaterialColor generateMaterialColor(Color color) {
    return MaterialColor(color.value, {
      50: tintColor(color, 0.9),
      100: tintColor(color, 0.8),
      200: tintColor(color, 0.6),
      300: tintColor(color, 0.4),
      400: tintColor(color, 0.2),
      500: color,
      600: shadeColor(color, 0.1),
      700: shadeColor(color, 0.2),
      800: shadeColor(color, 0.3),
      900: shadeColor(color, 0.4),
    });
  }

  int tintValue(int value, double factor) =>
      max(0, min((value + ((255 - value) * factor)).round(), 255));

  Color tintColor(Color color, double factor) => Color.fromRGBO(
      tintValue(color.red, factor),
      tintValue(color.green, factor),
      tintValue(color.blue, factor),
      1);

  int shadeValue(int value, double factor) =>
      max(0, min(value - (value * factor).round(), 255));

  Color shadeColor(Color color, double factor) => Color.fromRGBO(
      shadeValue(color.red, factor),
      shadeValue(color.green, factor),
      shadeValue(color.blue, factor),
      1);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Scratch games',
      theme: ThemeData(
        primarySwatch: generateMaterialColor(Colors.black),
      ),
      home: const MenuPage(),
    );
  }
}

class MenuPage extends StatelessWidget {
  const MenuPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: Stack(
      children: [
        Image.asset(
          "assets/background1.jpg",
          fit: BoxFit.fitWidth,
          width: size.width,
          height: size.height,
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(children: [
            Text(
              "Loto Saint Valentin pour les nons valentins 🤪",
              style: TextStyle(fontSize: 40),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 40,
            ),
            ListView(
              shrinkWrap: true,
              children: [
                Card(
                    child: ListTile(
                  title: const Text("Jeux #1",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                  onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => ScratchPage())),
                )),
                Card(
                    child: ListTile(
                  title: const Text("Jeux #2",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                  onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => ScratchPage2())),
                )),
              ],
            ),
            Flexible(
                child: Stack(
              fit: StackFit.expand,
              children: [
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: 300,
                      height: 300,
                      child: Lottie.asset('assets/rose.json',
                          animate: true, repeat: true),
                    )),
              ],
            )),
          ]),
        )
      ],
    ));
  }
}

class RandomPage extends StatefulWidget {
  const RandomPage({Key? key}) : super(key: key);

  @override
  _RandomPageState createState() => _RandomPageState();
}

class _RandomPageState extends State<RandomPage> {
  late List<Widget> _widgets;
  final _animationController = AnimationController();

  @override
  void initState() {
    _widgets = [
      Container(key: const ValueKey(1), color: Colors.pink),
      Container(key: const ValueKey(2), color: Colors.red),
      Container(key: const ValueKey(3), color: Colors.green),
      Container(key: const ValueKey(4), color: Colors.blue)
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(title: const Text("Random")),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          _animationController.start();
        },
      ),
      body: Center(
          child: Column(
        children: [
          AspectRatio(
            aspectRatio: 3 / 2,
            child: SizedBox(
                width: width,
                child: RandomAnimatedWidget(
                  animationController: _animationController,
                  widgets: _widgets,
                )),
          )
        ],
      )),
    );
  }
}

class RandomAnimatedWidget extends StatefulWidget {
  final List<Widget> widgets;
  final AnimationController? animationController;

  const RandomAnimatedWidget(
      {required this.widgets, this.animationController, Key? key})
      : super(key: key);

  @override
  _RandomAnimatedWidgetState createState() => _RandomAnimatedWidgetState();
}

class _RandomAnimatedWidgetState extends State<RandomAnimatedWidget> {
  late Timer _animationTimer;
  late Timer _randomTimer;
  int _index = 0;

  @override
  void initState() {
    widget.animationController?.addListener(() {
      if (widget.animationController?.isPlaying == true) {
        start();
      }
    });
    super.initState();
  }

  void start() {
    _randomTimer = Timer(const Duration(seconds: 10), () {
      _animationTimer.cancel();
    });
    _animationTimer = Timer.periodic(const Duration(milliseconds: 1000), (_) {
      setState(() {
        _index = (_index + 1) % widget.widgets.length;
      });
    });
  }

  void stop() {}

  @override
  void dispose() {
    _animationTimer.cancel();
    _randomTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      transitionBuilder: (child, animation) {
        return ScaleTransition(
          scale: animation,
          child: child,
        );
      },
      duration: const Duration(milliseconds: 500),
      child: widget.widgets[_index],
    );
  }
}

class RandomWidget extends StatefulWidget {
  final List<Widget> widgets;
  final AnimationController? animationController;

  const RandomWidget(
      {required this.widgets, this.animationController, Key? key})
      : super(key: key);

  @override
  _RandomWidgetState createState() => _RandomWidgetState();
}

class _RandomWidgetState extends State<RandomWidget> {
  late Timer _animationTimer;
  late Timer _randomTimer;
  int _index = 0;

  @override
  void initState() {
    widget.animationController?.addListener(() {
      if (widget.animationController?.isPlaying == true) {
        start();
      }
    });
    super.initState();
  }

  void start() {
    _randomTimer = Timer(const Duration(seconds: 10), () {
      _animationTimer.cancel();
    });
    _animationTimer = Timer.periodic(const Duration(milliseconds: 1000), (_) {
      setState(() {
        _index = (_index + 1) % widget.widgets.length;
      });
    });
  }

  void stop() {}

  @override
  void dispose() {
    _animationTimer.cancel();
    _randomTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: _index,
      children: widget.widgets,
    );
  }
}

class ScratchPage extends StatefulWidget {
  const ScratchPage({Key? key}) : super(key: key);

  @override
  State<ScratchPage> createState() => _ScratchPageState();
}

class _ScratchPageState extends State<ScratchPage> {
  late ConfettiController _controllerCenter;
  late ConfettiController _controllerCenterRight;
  late ConfettiController _controllerCenterLeft;
  late ConfettiController _controllerTopCenter;
  late ConfettiController _controllerBottomCenter;
  bool _showDetail = false;

  @override
  void initState() {
    super.initState();
    _controllerCenter =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerCenterRight =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerCenterLeft =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerTopCenter =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerBottomCenter =
        ConfettiController(duration: const Duration(seconds: 10));
  }

  @override
  void dispose() {
    _controllerCenter.dispose();
    _controllerCenterRight.dispose();
    _controllerCenterLeft.dispose();
    _controllerTopCenter.dispose();
    _controllerBottomCenter.dispose();
    super.dispose();
  }

  Path drawStar(Size size) {
    // Method to convert degree to radians
    double degToRad(double deg) => deg * (pi / 180.0);

    const numberOfPoints = 15;
    final halfWidth = size.width / 2;
    final externalRadius = halfWidth;
    final internalRadius = halfWidth / 2.5;
    final degreesPerStep = degToRad(360 / numberOfPoints);
    final halfDegreesPerStep = degreesPerStep / 2;
    final path = Path();
    final fullAngle = degToRad(360);
    path.moveTo(size.width, halfWidth);

    for (double step = 0; step < fullAngle; step += degreesPerStep) {
      path.lineTo(halfWidth + externalRadius * cos(step),
          halfWidth + externalRadius * sin(step));
      path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
          halfWidth + internalRadius * sin(step + halfDegreesPerStep));
    }
    path.close();
    return path;
  }

  _showDialogDetail() {
    showDialog(
        context: context,
        builder: (context) {
          return Container(
            child: Center(child: Image.asset("assets/chaussettes.jpg")),
          );
        });
  }

  _showDialogGame1() {
    showDialog(
        context: context,
        builder: (context) {
          //MediaQuery.of(context).size.width;
          return AlertDialog(
              content: Container(
            width: 300,
            height: 500,
            child: Center(
                child: Column(
              children: [
                Container(
                  height: 150,
                  width: double.infinity,
                  child: Image.asset(
                    "assets/ferdinand.jpg",
                    width: 280,
                    height: 180,
                  ),
                ),
                SizedBox(height: 32),
                Text(
                  "On se retrouve à Arcachon et on se fait un resto pour bien démarrer les vacances !!!\nEnsuite tu restes (tu penses à bien fermer les fenêtres en partant 😆 )\nLe lendemain on est prêt pour partir à la fraiche !\nOn peut aussi faire la soirée à bordeaux si tu préféres...",
                  style: TextStyle(fontSize: 20),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: 30),
                MaterialButton(
                  child: Text("Fermer la fenêtre"),
                  onPressed: () => Navigator.of(context).pop(),
                )
              ],
            )),
          ));
        });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              "assets/background2.png",
              fit: BoxFit.fill,
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Text(
                  "Grattez pour découvrir votre cadeaux",
                  style: TextStyle(color: Colors.white, fontSize: 24),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: Visibility(
                      visible: _showDetail,
                      child: AnimatedTextKit(
                        onTap: _showDialogGame1,
                        animatedTexts: [
                          TypewriterAnimatedText(
                            'Découvrir le gain',
                            textStyle: const TextStyle(
                              fontSize: 24.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                            speed: const Duration(milliseconds: 150),
                          ),
                        ],
                        totalRepeatCount: 1,
                        pause: const Duration(milliseconds: 1000),
                        displayFullTextOnTap: false,
                        stopPauseOnTap: false,
                      ),
                    ))),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 80.0),
                child: Container(
                    clipBehavior: Clip.antiAlias,
                    decoration: const BoxDecoration(
                        color: Colors.white, shape: BoxShape.circle),
                    child: ScratchArea(
                      text: "Alexia\n c'est gagné !\n Une soirée ce soir",
                      color: Colors.black,
                      backgroundColor: Colors.white,
                      width: 150,
                      texture: ScratchTexture.gold,
                      height: 150,
                      onThreshold: () {
                        setState(() {
                          _showDetail = true;
                        });
                        _controllerCenter.play();
                      },
                    )),
              ),
            ),
            ConfettiWidget(
              confettiController: _controllerCenter,
              blastDirectionality: BlastDirectionality
                  .explosive, // don't specify a direction, blast randomly
              shouldLoop:
                  true, // start again as soon as the animation is finished
              colors: const [
                Colors.green,
                Colors.blue,
                Colors.pink,
                Colors.orange,
                Colors.purple
              ], // manually specify the colors to be used
              createParticlePath: drawStar, // define a custom shape/path.
            ),
          ],
        ));
  }

  @override
  Widget buildAudrey(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              "assets/card3.png",
              fit: BoxFit.fill,
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: Visibility(
                      visible: _showDetail,
                      child: AnimatedTextKit(
                        onTap: _showDialogDetail,
                        animatedTexts: [
                          TypewriterAnimatedText(
                            'Découvrir le gain',
                            textStyle: const TextStyle(
                              fontSize: 24.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                            speed: const Duration(milliseconds: 150),
                          ),
                        ],
                        totalRepeatCount: 1,
                        pause: const Duration(milliseconds: 1000),
                        displayFullTextOnTap: false,
                        stopPauseOnTap: false,
                      ),
                    ))),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 80.0),
                child: Container(
                    clipBehavior: Clip.antiAlias,
                    decoration: const BoxDecoration(
                        color: Colors.white, shape: BoxShape.circle),
                    child: ScratchArea(
                      text: "Audrey\n c'est Gagné !\n Une paire de chaussettes",
                      color: Colors.black,
                      backgroundColor: Colors.white,
                      //image: Image.asset(
                      //"assets/card1.png",
                      //fit: BoxFit.fill,
                      //),
                      width: 150,
                      texture: ScratchTexture.gold,
                      height: 150,
                      onThreshold: () {
                        setState(() {
                          _showDetail = true;
                        });
                        AssetsAudioPlayer.newPlayer().open(
                          Audio("assets/happy.mp3"),
                          autoStart: true,
                          showNotification: true,
                        );
                        _controllerCenter.play();
                      },
                    )),
              ),
            ),
            ConfettiWidget(
              confettiController: _controllerCenter,
              blastDirectionality: BlastDirectionality
                  .explosive, // don't specify a direction, blast randomly
              shouldLoop:
                  true, // start again as soon as the animation is finished
              colors: const [
                Colors.green,
                Colors.blue,
                Colors.pink,
                Colors.orange,
                Colors.purple
              ], // manually specify the colors to be used
              createParticlePath: drawStar, // define a custom shape/path.
            ),
          ],
        ));
  }

  @override
  Widget buildAlex(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: [
        Image.asset(
          "assets/card1.png",
          fit: BoxFit.fill,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 80.0),
            child: Scratcher(
              accuracy: ScratchAccuracy.low,
              brushSize: 20,
              threshold: 40,
              image: Image.asset("assets/argent1.jpg"),
              //color: Colors.red,
              //onChange: (value) => print("Scratch progress: $value%"),
              onThreshold: () {
                _controllerCenter.play();
              },
              child: Container(
                width: 200,
                height: 80,
                color: Colors.white,
                child: Center(
                  child: const Text(
                    "Gagné !!!\n"
                    "Repas au Maè Tû",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ),
        ),
        ConfettiWidget(
          confettiController: _controllerCenter,
          blastDirectionality: BlastDirectionality
              .explosive, // don't specify a direction, blast randomly
          shouldLoop: true, // start again as soon as the animation is finished
          colors: const [
            Colors.green,
            Colors.blue,
            Colors.pink,
            Colors.orange,
            Colors.purple
          ], // manually specify the colors to be used
          createParticlePath: drawStar, // define a custom shape/path.
        ),
      ],
    ));
  }
}

class ScratchPage2 extends StatefulWidget {
  const ScratchPage2({Key? key}) : super(key: key);

  @override
  State<ScratchPage2> createState() => _ScratchPage2State();
}

class _ScratchPage2State extends State<ScratchPage2> {
  late ConfettiController _controllerCenter;
  late ConfettiController _controllerCenterRight;
  late ConfettiController _controllerCenterLeft;
  late ConfettiController _controllerTopCenter;
  late ConfettiController _controllerBottomCenter;
  bool _showDetail = false;

  @override
  void initState() {
    super.initState();
    _controllerCenter =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerCenterRight =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerCenterLeft =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerTopCenter =
        ConfettiController(duration: const Duration(seconds: 10));
    _controllerBottomCenter =
        ConfettiController(duration: const Duration(seconds: 10));
  }

  @override
  void dispose() {
    _controllerCenter.dispose();
    _controllerCenterRight.dispose();
    _controllerCenterLeft.dispose();
    _controllerTopCenter.dispose();
    _controllerBottomCenter.dispose();
    super.dispose();
  }

  Path drawStar(Size size) {
    // Method to convert degree to radians
    double degToRad(double deg) => deg * (pi / 180.0);

    const numberOfPoints = 15;
    final halfWidth = size.width / 2;
    final externalRadius = halfWidth;
    final internalRadius = halfWidth / 2.5;
    final degreesPerStep = degToRad(360 / numberOfPoints);
    final halfDegreesPerStep = degreesPerStep / 2;
    final path = Path();
    final fullAngle = degToRad(360);
    path.moveTo(size.width, halfWidth);

    for (double step = 0; step < fullAngle; step += degreesPerStep) {
      path.lineTo(halfWidth + externalRadius * cos(step),
          halfWidth + externalRadius * sin(step));
      path.lineTo(halfWidth + internalRadius * cos(step + halfDegreesPerStep),
          halfWidth + internalRadius * sin(step + halfDegreesPerStep));
    }
    path.close();
    return path;
  }

  _showDialogGame1() {
    showDialog(
        context: context,
        builder: (context) {
          //MediaQuery.of(context).size.width;
          return AlertDialog(
              content: Container(
            width: 300,
            height: 500,
            child: Center(
                child: Column(
              children: [
                Container(
                  height: 150,
                  width: double.infinity,
                  child: Image.asset(
                    "assets/chalet.jpeg",
                    width: 280,
                    height: 180,
                  ),
                ),
                SizedBox(height: 32),
                Text(
                  "Vous gagnez une semaine au Ski au mois de mars. La destination n'est pas encore trouvée mais ca sera super !!! \nVous pourrez retrouver vos sensations de glisse et vous détendre grace aux commodités de la station.\nDes supers vacances à venir !!!",
                  style: TextStyle(fontSize: 20),
                  textAlign: TextAlign.left,
                ),
                SizedBox(height: 40),
                MaterialButton(
                  child: Text("Fermer la fenêtre"),
                  onPressed: () => Navigator.of(context).pop(),
                )
              ],
            )),
          ));
        });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> gifts = [
      Image.asset("assets/love2.png"),
      Image.asset("assets/roses.png"),
      Image.asset("assets/iphone.jpg"),
      Image.asset("assets/vase.jpeg"),
      Image.asset("assets/love2.png"),
      Image.asset("assets/chalet.jpeg"),
      Image.asset("assets/maison.jpeg"),
      Image.asset("assets/chalet.jpeg"),
      Image.asset("assets/iphone.jpg"),
      Image.asset("assets/roses.png"),
      Image.asset("assets/vase.jpeg"),
      Image.asset(
        "assets/chalet.jpeg",
        key: Key("chalet"),
      ),
    ];
    return Scaffold(
        appBar: AppBar(),
        body: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              "assets/background3.jpg",
              fit: BoxFit.fill,
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(left: 12, right: 12, top: 40.0),
                child: Text(
                  "Trouvez 3 images identiques pour gagner le cadeaux.",
                  style: TextStyle(color: Colors.white, fontSize: 24),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: Visibility(
                      visible: _showDetail,
                      child: AnimatedTextKit(
                        onTap: _showDialogGame1,
                        animatedTexts: [
                          TypewriterAnimatedText(
                            'Découvrir le gain',
                            textStyle: const TextStyle(
                              fontSize: 24.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                            speed: const Duration(milliseconds: 150),
                          ),
                        ],
                        totalRepeatCount: 1,
                        pause: const Duration(milliseconds: 1000),
                        displayFullTextOnTap: false,
                        stopPauseOnTap: false,
                      ),
                    ))),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 80, left: 24, right: 24),
                child: GridView(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                            mainAxisSpacing: 20,
                            mainAxisExtent: 60,
                            crossAxisSpacing: 20,
                            crossAxisCount: 3),
                    children: gifts.map((e) {
                      return Container(
                          child: ScratchArea(
                        widget: e,
                        color: Colors.black,
                        shape: ScratchShape.rectangle,
                        backgroundColor: Colors.white,
                        width: 200,
                        texture: ScratchTexture.silver,
                        height: 200,
                        onThreshold: () {
                          if (e.key == Key("chalet")) {
                            setState(() {
                              _showDetail = true;
                            });
                          }
                          _controllerCenter.play();
                        },
                      ));
                    }).toList()),
              ),
            ),
          ],
        ));
  }

  @override
  Widget buildAlex(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: [
        Image.asset(
          "assets/card1.png",
          fit: BoxFit.fill,
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 80.0),
            child: Scratcher(
              accuracy: ScratchAccuracy.low,
              brushSize: 20,
              threshold: 40,
              image: Image.asset("assets/argent1.jpg"),
              //color: Colors.red,
              //onChange: (value) => print("Scratch progress: $value%"),
              onThreshold: () {
                _controllerCenter.play();
              },
              child: Container(
                width: 200,
                height: 80,
                color: Colors.white,
                child: Center(
                  child: const Text(
                    "Gagné !!!\n"
                    "Repas au Maè Tû",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ),
        ),
        ConfettiWidget(
          confettiController: _controllerCenter,
          blastDirectionality: BlastDirectionality
              .explosive, // don't specify a direction, blast randomly
          shouldLoop: true, // start again as soon as the animation is finished
          colors: const [
            Colors.green,
            Colors.blue,
            Colors.pink,
            Colors.orange,
            Colors.purple
          ], // manually specify the colors to be used
          createParticlePath: drawStar, // define a custom shape/path.
        ),
      ],
    ));
  }
}

class AnimationController extends ChangeNotifier {
  bool isPlaying = false;

  void start() {
    isPlaying = true;
    notifyListeners();
  }

  void stop() {
    isPlaying = false;
    notifyListeners();
  }
}

class Gift {
  final String id;
  final String title;
  final String description;
  final String imageUrl;

  Gift(
      {required this.id,
      required this.title,
      required this.description,
      required this.imageUrl});
}

class GiftsRepository {
  late List<Gift> _gifts;

  GiftsRepository() {
    _gifts = [
      Gift(
          id: "1",
          title: "gift1",
          description: "gift1 desc",
          imageUrl: "image.jpg"),
      Gift(
          id: "1",
          title: "gift1",
          description: "gift1 desc",
          imageUrl: "image.jpg"),
      Gift(
          id: "1",
          title: "gift1",
          description: "gift1 desc",
          imageUrl: "image.jpg"),
      Gift(
          id: "1",
          title: "gift1",
          description: "gift1 desc",
          imageUrl: "image.jpg"),
    ];
  }

  List<Gift> gifts() {
    return _gifts;
  }
}

class RandomLocationWidget extends StatefulWidget {
  const RandomLocationWidget({Key? key}) : super(key: key);

  @override
  _RandomLocationWidgetState createState() => _RandomLocationWidgetState();
}

class _RandomLocationWidgetState extends State<RandomLocationWidget> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

enum ScratchShape { circle, rectangle }
enum ScratchTexture { gold, silver }

class ScratchArea extends StatelessWidget {
  final VoidCallback? onThreshold;
  final ScratchShape? shape;
  final double width;
  final double height;
  final String? text;
  final Image? image;
  final Widget? widget;
  final Color? backgroundColor;
  final Color? color;
  final ScratchTexture? texture;
  const ScratchArea(
      {this.onThreshold,
      this.shape = ScratchShape.rectangle,
      this.texture = ScratchTexture.gold,
      this.backgroundColor = Colors.white,
      this.color = Colors.black,
      required this.width,
      required this.height,
      this.text,
      this.image,
      this.widget,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scratcher(
      accuracy: ScratchAccuracy.low,
      brushSize: 20,
      threshold: 40,
      image: Image.asset(texture == ScratchTexture.gold
          ? "assets/gold.jpg"
          : "assets/silver.jpg"),
      onThreshold: () {
        onThreshold?.call();
      },
      child: Container(
          decoration: BoxDecoration(
              color: backgroundColor,
              shape: shape == ScratchShape.circle
                  ? BoxShape.circle
                  : BoxShape.rectangle),
          width: width,
          height: height,
          child: Center(
            child: _widget(),
          )),
    );
  }

  Widget _widget() {
    final textWidget = Text(text == null ? "" : text!,
        textAlign: TextAlign.center,
        style:
            TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: color));

    if (image != null) {
      return Stack(
        fit: StackFit.expand,
        children: [
          image!,
          if (text != null)
            Align(alignment: Alignment.center, child: textWidget),
        ],
      );
    }

    if (widget != null) {
      return widget!;
    }

    return textWidget;
  }
}
